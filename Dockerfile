ARG REPO_ARCH

FROM vectorim/element-web:ELEMENT_VERSION as builder

FROM $REPO_ARCH/nginx:mainline-alpine

RUN \
  apk add tini && \
  sed -i '3i\ \ \ \ application/wasm wasm\;' /etc/nginx/mime.types

COPY --from=builder /app /app
COPY init.sh /usr/local/bin/init.sh

RUN \
  rm -rf /usr/share/nginx/html && \
  ln -s /app /usr/share/nginx/html && \
  mkdir /etc/riot-web

VOLUME /etc/riot-web

ENTRYPOINT ["/sbin/tini", "--"]
CMD ["init.sh", "nginx", "-g", "daemon off;"]
