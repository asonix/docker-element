#!/usr/bin/env sh

set -xe

cp /etc/riot-web/config.json /app/config.json

exec "$@"
